const cacheName = 'v1';
const cacheFiles = [
    './',
    './index.html',
    './index.js',
    './css/poole.css',
    './css/lanyon.css',
    './posts/index.html',
    './posts/building-an-api/index.html',
    './posts/the-new-stack/index.html',
    './posts/monkey-patching/index.html'
]

self.addEventListener('install', e => {
    e.waitUntil(
        caches.open(cacheName).then(cache => {
            return cache.addAll(cacheFiles);
        })
    );
});

self.addEventListener('activate', e => {
    e.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(cacheNames.map(thisCacheName => {
                if (thisCacheName !== cacheName) {

                    return caches.delete(thisCacheName);
                }
            }));
        })
    );
});

self.addEventListener('fetch', event => {
    event.respondWith(fromNetwork(event.request, 400).catch(() => {
        return fromCache(event.request);
    }));
});

function fromNetwork(request, timeout) {
    return new Promise((resolve, reject) => {
        const timeoutId = setTimeout(reject, timeout);

        fetch(request).then(response => {
            clearTimeout(timeoutId);
            resolve(response);
        }, reject);
    })
}

async function fromCache(request) {
    const cache = await caches.open(cacheName)
    const match = await cache.match(request)
    return match || Promise.reject('no-match');
}